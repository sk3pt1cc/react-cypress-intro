import axios from 'axios';

export const login = (username, password) => axios.post('/login', {
  body: JSON.stringify({
    username, password,
  })
})