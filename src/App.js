import React from 'react';
import { login } from './http';
import './App.css';

const App = () => {
  const [user, setUser] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [error, setError] = React.useState(null);
  const [success, setSuccess] = React.useState(null);

  const onSubmit = (e) => {
    e.preventDefault();
  
    login(user, password).then(() => {
      setSuccess('Successful login.');
    }).catch(() => {
      setError('Incorrect details.')
    });
  };

  return (
    <div className="App">
      <div className="notification">
        {success && (
          <div className="success">
            {success}
            <div className="clear-notification" onClick={() => setSuccess(null)}>x</div>
          </div>
        )}
        {error && (
          <div className="error">
            {error}
            <div className="clear-notification" onClick={() => setError(null)}>x</div>
          </div>
        )}
      </div>
      <div className="login">
        <form onSubmit={onSubmit}>
          <div className="form-item">
            <label htmlFor="username">
              Username
              <input type="text" id="username" value={user} onChange={e => setUser(e.target.value)} />
            </label>
          </div>
          <div className="form-item">
            <label htmlFor="password">
              Password
              <input type="text" id="password" value={password} onChange={e => setPassword(e.target.value)} />
            </label>
          </div>
          <div className="form-item btn">
            <button id="login" type="submit">
              Login
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default App;
